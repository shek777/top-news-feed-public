import "package:flutter/material.dart";
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
//import 'package:flutter_web_view/flutter_web_view.dart';

//import './pages/landing_page.dart';


//region body

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
//import 'package:flutter/material.dart';


Future<Map<String, dynamic>> getNews() async {
  String apiKey = "b0b1ef9adea14a38815e55baef6b7b38";
  // ignore: unnecessary_brace_in_string_interps
  String apiUrl = 'https://newsapi.org/v2/top-headlines?country=us&category=entertainment&pageSize=100&apiKey=$apiKey';
  // Make a HTTP GET request to the CoinMarketCap API.
  // Await basically pauses execution until the get() function returns a Response
  http.Response response = await http.get(apiUrl);
  // Using the JSON class to decode the JSON String
  return json.decode(response.body);
}

// This app is a stateful, it tracks the user's current choice.
class Home extends StatefulWidget {


  List news;

  Home(this.news);

  @override
  _BasicAppBarSampleState createState() => new _BasicAppBarSampleState(news);
}

class _BasicAppBarSampleState extends State<Home> {
  // Choice _selectedChoice = choices[0]; // The app's "state".

  List _topnews;


  void _getData() async {

    Map<String, dynamic> news = await getNews();

    var temp = news['articles'] as List;
    //if(news.isNotEmpty) {

    setState(() => _topnews = temp );

    // }

  }



  _BasicAppBarSampleState(this._topnews);

  // void _select(Choice choice) {
  // Causes the app to rebuild with the new _selectedChoice.
  //  setState(() {
  // _selectedChoice = choice;
  //   });
  // }
  @override
  void initState() {
    // _getData();
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.deepOrangeAccent,
          title: const Text('Top News'),
    leading: new IconButton(
    tooltip: 'Top News Feed',
    icon: const Icon(Icons.language),),
          // actions: <Widget>[
          // action button
          //  new IconButton(
          //    icon: new Icon(choices[0].icon),
          //    onPressed: () {
          //     _select(choices[0]);
          //   },
          //   ),
          // action button
          //   new IconButton(
          //    icon: new Icon(choices[1].icon),
          ////    onPressed: () {
          //      _select(choices[1]);
          //    },
          //  ),
          // overflow menu
          //   new PopupMenuButton<Choice>(
          //     onSelected: _select,
          //     itemBuilder: (BuildContext context) {
          //       return choices.skip(2).map((Choice choice) {
          //         return new PopupMenuItem<Choice>(
          //           value: choice,
          //           child: new Text(choice.title),
          //         );
          //       }).toList();
          //     },
          //   ),
          //  ],
        ),
        body: _buildBody(_topnews),
        floatingActionButton: new FloatingActionButton(
          onPressed:() { _getData();},
          backgroundColor: Colors.greenAccent,
          mini: false,
          child: new Icon(Icons.autorenew),
        ),

        // body: new Padding(
        //   padding: const EdgeInsets.all(16.0),
        //  child: new ChoiceCard(choice: _selectedChoice),
        // ),
      ),
    );
  }
}


Widget _buildBody(List topnews) {

  // final List topnews;



  return new Container(
    // A top margin of 56.0. A left and right margin of 8.0. And a bottom margin of 0.0.
    margin: const EdgeInsets.fromLTRB(8.0, 5.0, 8.0, 0.0),
    child: new Column(
      // A column widget can have several widgets that are placed in a top down fashion
      children: <Widget>[

        _getListViewWidget(topnews)
      ],
    ),
  );
}

Widget _getListViewWidget(List topnews) {


  // We want the ListView to have the flexibility to expand to fill the
  // available space in the vertical axis
  return new Flexible(
      child: new ListView.builder(
        // The number of items to show
          itemCount: topnews.length,
          // Callback that should return ListView children
          // The index parameter = 0...(itemCount-1)
          itemBuilder: (context, index) {
            // Get the currency at this position
            final Map news = topnews[index];

            // Get the icon color. Since x mod y, will always be less than y,
            // this will be within bounds
            // final MaterialColor color = _colors[index % _colors.length];

            return _getListItemWidget(news);
          }));
}

Container _getListItemWidget(Map news) {
  // Returns a container widget that has a card child and a top margin of 5.0
  return new Container(
    margin: const EdgeInsets.only(top: 5.0),
    child: new Card(
      child: _getListTile(news),
    ),
  );
}

ListTileMod _getListTile(Map news) {
  return new ListTileMod(news: news);
}

class ListTileMod extends StatelessWidget {
  // Declare a field that holds the Todo
  final Map news;

  // In the constructor, require a Todo
  ListTileMod({Key key, @required this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create our UI
    return new ListTile(
      //   leading: _getLeadingWidget(news['"urlToImage']),
      title: _getTitleWidget(news['title']),
      subtitle: _getSubtitleWidget(news['author'], news['description']),
      isThreeLine: true,
      enabled: true,
      onLongPress: () {
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => new DetailScreen(news: news)),
        );
      },
    );
  }
}

_respondToTap(String url) async {

  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

FadeInImage _getLeadingWidget(String urlToImage) {

  try {
    if(urlToImage.isNotEmpty) {
      return new FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: urlToImage,
      );
    }
  }catch(e){
    print(e.toString());

  }
}

Text _getTitleWidget(String title) {
  return new Text(
    title,
    style: new TextStyle(fontWeight: FontWeight.bold),
  );
}

Text _getSubtitleWidget(String author, String description) {
  return new Text('$author\n$description');
}

class DetailScreen extends StatelessWidget {
  // Declare a field that holds the Todo
  Map news;

  // In the constructor, require a Todo
  DetailScreen({Key key, @required this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create our UI
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(news['title']),
        backgroundColor: Colors.deepOrangeAccent,
          leading: new IconButton(
            tooltip: 'Top News Feed',
            icon: const Icon(Icons.language),)
      ),
      body: new Padding(
        padding: new EdgeInsets.all(16.0),
        child: detailedBodySection(news),
      ),
    );
  }
}

/*
class NewsListWidget extends StatelessWidget {

  // This is a list of material colors. Feel free to add more colors, it won't break the code
//  final List<MaterialColor> _colors = [Colors.blue, Colors.indigo, Colors.red];
  // The underscore before a variable name marks it as a private variable
  final List _news;

  // This is a constructor in Dart. We are assigning the value passed to the constructor
  // to the _currencies variable
  NewsListWidget(this._news);

  @override
  Widget build(BuildContext context) {
    // Build describes the widget in terms of other, lower-level widgets.
    return new Text('Hello World!');
  }

}
*/


Widget detailedBodySection(Map news){

  return new ListView(
    children: [
      ImageSection(news),
      titleSection(news),
      descriptionSection(news),
      buttonSection(news),
      //   textSection,
    ],
  );
}

Widget buttonSection(Map news) {
  return new ButtonSection(news:  news);
}


Widget titleSection(Map news) {

  return new TitleSection(news: news);
}


Widget descriptionSection(Map news) {

  return new DescriptionSection(news: news);
}



Widget ImageSection(Map news){

  if(news['urlToImage'].toString().isNotEmpty){
    return new Image.network(
      '${news['urlToImage']}',
      width: 600.0,
      height: 240.0,
      fit: BoxFit.cover,
    );
  }else{
    return new Text(
      'No image',
      style: new TextStyle(
        fontWeight: FontWeight.bold,
      ),
    );
  }
}


class TitleSection extends StatelessWidget {

  final Map news;

  // In the constructor, require a Todo
  TitleSection({Key key, @required this.news}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(32.0),
      child: new Row(
        children: [
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: new Text(
                    news['title']!= null? news['title']:"",
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                new Text(
                  news['author']!= null? news['author']:"",
                  style: new TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    //...
  }
}

class DescriptionSection extends StatelessWidget {

  final Map news;

  // In the constructor, require a Todo
  DescriptionSection({Key key, @required this.news}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    //...
    return new Container(
      padding: const EdgeInsets.fromLTRB(32.0,0.0,32.0,10.0),
      child: new Text(

        news['description'] != null? news['description'] : ""
        ,
        softWrap: true,
      ),
    );
    //...
  }
}

class ButtonSection extends StatelessWidget {

  final Map news;


  // In the constructor, require a Todo
  ButtonSection({Key key, @required this.news} ) : super(key: key) ;


  @override
  Widget build(BuildContext context) {
    //...

    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          buildButtonColumn1(Icons.arrow_back, 'BACK'),
          buildButtonColumn2(Icons.language, 'URL', news),

        ],
      ),
    );
    //...
  }
}


ColumnMod buildButtonColumn1(IconData icon, String label) {
  Color color = Colors.blue;


  return new ColumnMod(color: color, icon: icon, label: label,);
}

class ColumnMod extends StatelessWidget {

  Color color;
  IconData icon;
  String label;
  final Map news;

  // In the constructor, require a Todo
  ColumnMod({Key key, this.color, this.icon, this.label}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    //...
    return new Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        new GestureDetector(
          onTap: (){

            Navigator.pop(context);
          },
          child: new Icon(icon, color: color),
        ),
        new Container(
          margin: const EdgeInsets.only(top: 8.0),
          child: new Text(
            label,
            style: new TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );    //...
  }
}

ColumnMod1 buildButtonColumn2(IconData icon, String label, Map news) {
  Color color = Colors.blue;

  return new ColumnMod1(color: color, icon: icon, label: label, news: news);

}
//...


class ColumnMod1 extends StatelessWidget {

  Color color;
  IconData icon;
  String label;
  final Map news;

  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  @override
  void initState() {


    flutterWebviewPlugin.close();

  }

  @override
  void dispose() {
    flutterWebviewPlugin.dispose();
     }

  // In the constructor, require a Todo
  ColumnMod1({Key key, this.color, this.icon, this.label, this.news}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    //...
    return new Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        new GestureDetector(
          onTap: (){

           // _respondToTap(news['url']);

            /*Navigator.push(
              context,
       ggg       new MaterialPageRoute(builder: (context) => new DetailScreen(news: news['url'])),
            );*/
            if(news['url'].toString() != " ") {
              flutterWebviewPlugin.launch(news['url'], withJavascript: true,
                  clearCookies: false,
                  hidden: false,
                  enableAppScheme: true,
                  rect: null,
                  userAgent: null,
                  withZoom: true,
                  withLocalStorage: true);
            }

          },
          child: new Icon(icon, color: color),
        ),
        new Container(
          margin: const EdgeInsets.only(top: 8.0),
          child: new Text(
            label,
            style: new TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );//...
  }
}


// Web View

/*
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Webview Example',
      theme: ThemeData.dark(),
      routes: {
        "/": (_) => Home(),
        "/webview": (_) => WebviewScaffold(
          url: url,
          appBar: AppBar(
            title: Text("WebView"),
          ),
          withJavascript: true,
          withLocalStorage: true,
          withZoom: true,
        )
      },

      // home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  final webView = FlutterWebviewPlugin();
  TextEditingController controller = TextEditingController(text: url);

  @override
  void initState() {
    super.initState();

    webView.close();
    controller.addListener(() {
      url = controller.text;
    });
  }

  @override
  void dispose() {
    webView.dispose();
    controller.dispose();
    super.dispose();
  }

  // Future launchURL(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(url, forceSafariVC: true, forceWebView: true);
  //   } else {
  //     print("Can't Launch ${url}");
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("WebView"),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10.0),
                child: TextField(
                  controller: controller,
                ),
              ),
              RaisedButton(
                child: Text("Open Webview"),
                onPressed: () {
                  Navigator.of(context).pushNamed("/webview");
                },
              )
            ],
          ),
        )

      // body: Center(
      //   child: Column(
      //     children: <Widget>[
      //       Container(
      //         padding: EdgeInsets.all(10.0),
      //         child: Text(URL),
      //       ),
      //       RaisedButton(
      //         child: Text("Open Link"),
      //         onPressed: () {
      //           launchURL(URL);
      //         },
      //       )
      //     ],
      //   ),
      // ),
    );
  }
}
*/

//endregion

class Article
{
  final Source source;
  final String author;
  final String title;
  final String description;
  final String url;
  final String urlToImage;
  final DateTime publishedAt;

  Article(this.source, this.author, this.title, this.description, this.url, this.urlToImage, this.publishedAt);

  Article.fromJson(Map<String, dynamic> json)
      : author = json['author'],
        title = json['title'],
        description = json['description'],
        url = json['url'],
        urlToImage = json['urlToImage'],
        publishedAt = json['publishedAt'] as DateTime,
        source = json['source'] as Source;

  Map<String, dynamic> toJson() =>
      {
        'author': author,
        'title': title,
        'description': description,
        'url': url,
        'urlToImage': urlToImage,
        'DateTime': publishedAt.toString(),
        'source': source.toJson(),
      };
}

class Source
{
  final String id;
  final String name;
  final String description;
  final String url;
  final String category;
  final String language;
  final String country;

  Source(this.id, this.name, this.description, this.url, this.category, this.language, this.country);

  Source.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        description = json['description'],
        url = json['url'],
        category = json['category'],
        language = json['language'],
        country = json['country'];

  Map<String, dynamic> toJson() =>
      {
        'id': id,
        'name': name,
        'description': description,
        'url': url,
        'category': category,
        'language': language,
        'country': country,
      };
}

class NewsRootObject
{
  final String status;
  final int totalResults;
  // final List<Source> sources;
  final List<Article> articles;

  NewsRootObject(this.status, this.totalResults, this.articles);

  NewsRootObject.fromJson(Map<String, dynamic> json)
      : status = json['status'],
        totalResults = json['totalResults'],
  //  sources = json['source'],
        articles = json['articles'] as List;

  Map<String, dynamic> toJson() =>
      {
        'status': status,
        'totalResults': totalResults,
        //  'source': sources, // TODO: convert to an array of sources, or a single json object
        'articles': articles, // TODO: convert to an array of sources, or a single json object
      };

}

List<Article> _parseJsonForArticles(String json){
  List<Article> Articles = new List<Article>();
  // for (var article in json.decode(json)) {
  //  Articles.add(new Article(null, article['author'], article['title'], article['description'']', article['url'], article['urlToImage'], article['publishedAt']));
  // }

  return Articles;
}

//endregion

//test code in main
// Before printing it to the Console
//   var testlist = news['articles'] as List;
//  print(news['articles'][1]['source']);
//  print(testlist[1]['source']['id']);
//  print(testlist.length);
// print('{test: me}');

void main() async {

  List errorMsg = new List();
  errorMsg = [];

  try {



    Map<String, dynamic> news = await getNews();
    var listOfArticles = news['articles'] as List;
    runApp(new Home(listOfArticles));

  } catch (e) {

    errorMsg.add({
      "source": {
        "id": "",
        "name": ""
      },
      "author": " ",
      "title": "No data",
      "description": "Oops, something went wrong.",
      "url": " ",
      "urlToImage": "",
      "publishedAt": ""

    });

    runApp(new Home(errorMsg));
    print(e.toString());
  }
}


//https://github.com/iampawan/FlutterExampleApps